# Testing samples

## Start mock server for a simple API
    
    ./mockserver -b modules/apibms-core/src/test/resources/api1.md
    
## Start mock server for more complex APIs
There are many example Blueprints on the API Blueprint [Github project](https://github.com/apiaryio/api-blueprint/tree/master/examples).

See some of the example API blueprints in:

    modules/apibms-standalone/src/test/resources/examples/
    
Example:

    ./mockserver -b modules/apibms-standalone/src/test/resources/examples/1.md
