/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core;

import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.service.ExecutionService;
import com.gatehill.apibms.core.service.ServerService;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Wraps the parsing of an API Blueprint and creation of a server instance hosting the mock definition.
 *
 * @author pete
 */
class MockServerApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockServerApplication.class);

    @Inject
    private ExecutionService executionService;

    @Inject
    private ServerService serverService;

    /**
     * Parse an API blueprint and create a server instance listening on <code>host</code> on port <code>port</code>
     * hosting the mock definition.
     *
     * @param blueprintFile the API Blueprint file
     * @param format        the format of the API Blueprint
     * @param host          the host to which to bind the server
     * @param port          the port on which to listen for connections
     * @return the return code for the operation, where <code>0</code> indicates success
     */
    public int runApplication(File blueprintFile, MockFactory.BlueprintFormat format, String host, Integer port) {
        // start server from blueprint
        final ExecutionInstance exec =
                executionService.execute(blueprintFile, format, host, port);

        LOGGER.info("Starting server on port {}", exec.getPort());

        // print endpoints
        final StringBuilder sbEndpoints = new StringBuilder();
        sbEndpoints.append("Endpoints:");
        for (ResourceDefinition endpoint : exec.getEndpoints()) {
            sbEndpoints.append(String.format("\r\n(%s) http://%s:%s%s",
                    listRequestMethods(endpoint), exec.getHost(), exec.getPort(), endpoint.getUrl()));
        }
        LOGGER.info(sbEndpoints.toString());
        LOGGER.info("Add ?blueprint to the end of an endpoint to see its documentation");

        try {
            System.out.println("Press Enter to stop...");
            System.in.read();

        } catch (IOException e) {
            LOGGER.error("Error waiting for cancel");
        }

        // stop server
        System.out.println("Stopping...");
        serverService.stop(exec);

        return 0;
    }

    /**
     * List the request methods for the given ResourceDefinition.
     *
     * @param endpoint the ResourceDefinition whose requests will be listed
     * @return a comma-separated list of request verbs
     */
    private String listRequestMethods(ResourceDefinition endpoint) {
        final StringBuilder sb = new StringBuilder();

        for (RequestDefinition request : endpoint.getRequests()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(request.getVerb());
        }

        return sb.toString();
    }
}
