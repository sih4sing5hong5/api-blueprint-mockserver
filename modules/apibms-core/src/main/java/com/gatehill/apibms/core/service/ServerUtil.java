package com.gatehill.apibms.core.service;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Utilty methods.
 *
 * @author pete
 */
public class ServerUtil {
    /**
     * Don't allow this class to be instantated.
     */
    private void ServerUtil() {
    }

    /**
     * @return the number of a free listen port
     * @throws java.io.IOException
     */
    public static int getFreePort() throws IOException {
        // port gets closed upon completion of the try block
        try (final ServerSocket s = new ServerSocket(0)) {
            return s.getLocalPort();
        }
    }
}
