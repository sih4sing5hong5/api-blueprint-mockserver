/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.model.*;
import com.jayway.restassured.RestAssured;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by pete on 15/02/2014.
 */
public class ServerServiceTest {

    @InjectMocks
    private ServerServiceImpl service;

    @Mock
    private SettingsService settingsService;

    @Mock
    private ParsingService parsingService;

    @Before
    public void before() {
        service = new ServerServiceImpl();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void after() {
        Mockito.verifyNoMoreInteractions(settingsService, parsingService);
    }

    @Test
    public void testStartStop() throws Exception {
        final MockServer actual = getMockServer();

        // call endpoint
        RestAssured
                .get("http://localhost:" + actual.getPort() + "/message")
                .then()
                .assertThat()
                .statusCode(is(StatusCodes.OK))
                .and()
                .body(equalTo("Hello World!\n"));

        // stop
        service.stop(actual);
    }

    @Test
    public void testDocumentation() throws Exception {
        final MockServer actual = getMockServer();

        // mock behaviour
        when(settingsService.isDocumentationEnabled())
                .thenReturn(true);

        when(parsingService.parseMarkdownFile(isA(File.class)))
                .thenReturn("<html>");

        // call endpoint
        RestAssured
                .get("http://localhost:" + actual.getPort() + "/message?blueprint")
                .then()
                .assertThat()
                .statusCode(is(StatusCodes.OK))
                .and()
                .body(equalTo("<html>"));

        // stop
        service.stop(actual);

        // verify behaviour
        verify(settingsService).isDocumentationEnabled();
        verify(parsingService).parseMarkdownFile(isA(File.class));
    }

    private MockServer getMockServer() {
        // test data
        final MockDefinition mock = new MockDefinition();

        // fake blueprint file
        mock.setBlueprintFile(Mockito.mock(File.class));

        final ResourceDefinition endpoint = new ResourceDefinition("My API");
        mock.addEndpoint(endpoint);

        final RequestDefinition request = new RequestDefinition();
        request.setVerb("GET");

        endpoint.getRequests().add(request);
        endpoint.setUrl("/message");

        final ResponseDefinition response = new ResponseDefinition();
        endpoint.getResponses().put(StatusCodes.OK, response);

        response.getHeaders().put(Headers.CONTENT_TYPE_STRING, "text/plain");
        response.setCode(StatusCodes.OK);
        response.setBody("Hello World!\n");

        // call
        final MockServer actual = service.start(mock);

        // assert
        Assert.assertNotNull(actual);
        return actual;
    }
}
