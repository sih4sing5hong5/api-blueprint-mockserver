/*
   Copyright 2014 Pete Cornish

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package com.gatehill.apibms.core.service;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pete on 15/02/2014.
 */
public class ParsingServiceTest {

    private ParsingServiceImpl service;

    @Before
    public void before() {
        service = new ParsingServiceImpl();
    }

    @Test
    public void testParseMarkdownFile() {
        final File f = new File(ParsingServiceTest.class.getResource("/api1.md").getPath());

        // call
        final String actual = service.parseMarkdownFile(f);

        // assert
        Assert.assertNotNull(actual);
    }

    @Test
    public void testParseMarkdown() {
        final InputStream is = ParsingServiceTest.class.getResourceAsStream("/api1.md");

        // call
        final String actual = service.parseMarkdown(is);

        // assert
        Assert.assertNotNull(actual);
    }

    @Test
    public void testToXhtmlDocument() throws IOException {
        final String html = IOUtils.toString(ParsingServiceTest.class.getResourceAsStream("/api1.html"));

        // call
        final Document actual = service.toXhtmlDocument(html);

        // assert
        Assert.assertNotNull(actual);
    }
}
